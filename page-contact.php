<?php get_header(); ?>

<style>
    button,
    input,
    optgroup,
    select,
    textarea {
        margin: 20px;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    input {
        font-family: "Karla", Arial, sans-serif;
        font-weight: bold;
        font-size: 15px !important;
        line-height: 32px !important;
        text-transform: uppercase !important;
        padding: 15px !important;
        margin: 15px !important;
        background-color: #e7e8ec;
        border: solid 1px #e7e8ec !important;
        border-radius: 25px !important;
    }

    #envoyer {
        text-align: right;
        margin-left: 520px;
        padding: 5px;
    }

    /* ajout employe*/

    div.formulaire {
        margin: auto;
        display: flex;
        width: 100%;

    }

    .mail {
        width: 490px;
    }

    .envoyer {
        background-color: #E95734;
        font-size: 20px;
        padding: 10px 20px;
        -webkit-border-radius: 2px;
        border-radius: 30px;
    }


    .message {
        width: 480px;
        background-color: #e7e8ec !important;
        border: solid 1px #e7e8ec !important;
        border-radius: 25px !important;
        color: #172144 !important;
        font-family: "Karla", Arial, sans-serif;
        font-weight: bold;
        font-size: 15px !important;
        line-height: 32px !important;
        text-transform: uppercase !important;
        padding: 5px 30px !important;
    }
</style>

<?php

$nom = isset($_POST["nom"]) && !empty($_POST["nom"]) ? $_POST["nom"] : "";
$prenom = isset($_POST["prenom"]) && !empty($_POST["prenom"]) ? $_POST["prenom"] : "";
$ojet = isset($_POST["objet"]) && !empty($_POST["objet"]) ? $_POST["objet"] : "";
$mail = isset($_POST["mail"]) && !empty($_POST["mail"]) ? $_POST["mail"] : "";
$message = isset($_POST["message"]) && !empty($_POST["message"]) ? $_POST["message"] : "";
$tel = isset($_POST["tel"]) && !empty($_POST["tel"]) ? $_POST["tel"] : "";

$tout = $nom . "\n" . $prenom . "\n" . $mail . "\n" . $message;

if (isset($_POST['submit'])) {
    $ouvert = fopen('contact/' . $nom . '.txt', "w");
    fwrite($ouvert, $tout);
    fclose($ouvert);
    $merci = "<p class='message1'>Merci de votre message !</p>";
}
?>

<div style="background-color: white;" class="row">
    <div class="col-sm-6">
        <div style="border: 0px;" class="card">
            <div class="card-body">
                <h3 style="font-weight: bold;margin-left:150px;padding:20px" class="card-title">Ils se sont régalés </h3>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <?php
        $args = array(
            'post_type' => 'avis',
            'showposts' => 4,
        );
        $the_query = new WP_Query($args);
        while ($the_query->have_posts()) :
            $the_query->the_post();
        ?>

            <div style="border: 0px;" class="card">
                <div class="card-body">
                    <h5 style="color: red;font-weight:bold;font-size:80px" class="card-title"></h5>
                    <p class="card-text"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/text-quotes-.svg"><?php echo ' ' . get_the_content(); ?></p>
                    <p style="font-weight:bold"><?php echo get_the_title(); ?></p>
                </div>
            </div>

        <?php
        endwhile;
        ?>
    </div>
</div>

<h1 style="text-align: center;background-color:lightgreen;margin-bottom: -1px;font-size:50px">Nos Horaires</h1>
<div class="card-group">
    <div class="card">
        <div style="background-color: lightgreen;border:0px;margin-right: -10px;" class="card-body">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
            <img style="margin-left:130px" src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/Groupe 773.svg">
            <h5 style="font-size: 30px;line-height: 60px;color: #172144;" class="card-title">Sur place & à emporter</h5>
            <p style="font-size: 20px;line-height: 32px;color: #172144;margin:10px;" class="card-text">Ouvert au public du lundi au <br> vendredi de 11h30 à 19h et le <br> samedi jusqu'à 17h30</p>
        </div>
    </div>
    <div class="card">
        <div style="background-color: lightgreen;border:0px;margin-right: -10px;" style="background-color: lightgreen;" class="card-body">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
            <img style="margin-left:130px" src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/Groupe 774.svg">
            <h5 style="font-size: 30px;line-height: 60px;color: #172144;" class="card-title">Ouverture en soirée</h5>
            <p style="font-size: 20px;line-height: 32px;color: #172144;margin:10px;" class="card-text">Vous souhaitez organiser un <br> cocktail dinatoire, uen soirée <br> d'entreprise, un anniversaire ... <br>Parlons-en.</p>
        </div>
    </div>
    <div class="card">
        <div style="background-color: lightgreen;border:0px;margin-right: -10px;" class="card-body">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
            <img style="margin-left:130px" src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/bicycle (2).svg">
            <h5 style="font-size: 30px;line-height: 60px;color: #172144;" class="card-title">Livraison (prochainement...)</h5>
            <p style="font-size: 20px;line-height: 32px;color: #172144;margin:10px;" class="card-text">Faites-nous d'ores et déjà part <br> de vos besoins. Nous ferons tout <br> notre possible pour y réponde.
            </p>
        </div>
    </div>

</div>
<div style="background-color: lightgreen" class="commande">
</div>

<div style="background-color:white;border:0px" class="row">
    <div class="col-sm-4">
        <div style="border: 0px;margin-left:60px" class="card">
            <div class="card-body">
                <h3 style="font-weight: 700;font-size: 50px;line-height: 60px;color: #172144;" class="card-title">Géo-<br>bocalisation</h3>
                <p style="font-size: 20px;line-height: 32px;color: #172144;margin:10px;" class="card-text">Latitude: 47.995919 <br>Longitude : 0.202093<br>
                    02 43 78 25 11<br>contact@les-feles-du-bocal.bio<br>
                    198 rue Nationale <br>Place Washington <br> 72000 Le Mans</p><br>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div style="border: 0px;" class="card">
            <div style="border: 0px;" class="card-body">
                <iframe style="width:900px" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6242.15940430261!2d0.21011000000000002!3d47.9513!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sfr!2sfr!4v1593523926946!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> </div>
        </div>
    </div>
</div>


<h1 style="font-weight: bold;text-align: center;background-color:white;
     ">Nous contactez </h1>
<form action="" method="post">
    <div style=" background-color:white;" class="formulaire">
        <div class="row">
            <div style="margin-left: 250px;" class="col-sm-5">

                <input type="text" name="nom" placeholder="NOM">
                <input type="text" name="prenom" placeholder="PRÉNOM"><br>
                <input class="mail" type="email" name="mail" placeholder="E-MAIL"><br>
                <input class="mail" type="text" name="tel" placeholder="TELEPHONE">

            </div>

            <div class="col-sm-5">

                <input class="mail" type="text" name="objet" placeholder="OBJET">
                <textarea style="margin-left: 20px;" class="message " name="message" id="" cols="15" rows="5" placeholder="&#127925;  MESSAGE IN A BOCAL  &#127925;"></textarea>

            </div>

        </div>

    </div>

    <div class="display=flex">
        <input class="envoyer" type="submit" name="submit" value="ENVOYER">
    </div>
</form>


</body>

</html>

<?php get_footer(); ?>