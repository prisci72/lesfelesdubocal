<?php get_header(); ?>

<div class="container">
    <div style="display:block;margin-left: 300px;" class="row text-uppercase mb-5">
        <nav class="nav mt-3">
            <a style="margin-left: -320px;
                      border-right-style: solid;
                      margin-right: 280px;
                      border-right-width: 0px;" class="nav-link text-dark" href="page-concept.php">ACCUEIL / BOUTIQUE</a>
            <a class="nav-link text-dark border border-dark rounded-pill ml-5" href="#formules">FORMULES</a>
            <a class="nav-link text-dark border border-dark rounded-pill ml-3" href="#entrees">ENTRÉES</a>
            <a class="nav-link text-dark border border-dark rounded-pill ml-3" href="#plats">PLATS</a>
            <a class="nav-link text-dark border border-dark rounded-pill ml-3" href="#desserts">DESSERTS</a>
            <a class="nav-link text-dark border border-dark rounded-pill ml-3" href="#epicerie">EPICERIE</a>
        </nav>
    </div>

    <section id="formules" class="mb-5">
        <h2>Formules</h2>
        <div class="row">
            <?php
            $args = array(
                'post_type' => 'bocaux',
                'showposts' => 3,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'genre',
                        'field'    => 'slug',
                        'terms'    => 'formule',
                    ),
                ),
            );
            $the_query = new WP_Query($args);
            while ($the_query->have_posts()) :
                $the_query->the_post();
            ?>
                <div class="mt-3 col-md-6">
                    <div class="card border-0" style="width: 18rem;">
                        <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
                        <div style="background-color: lemonchiffon;" class="card-body">
                            <h5 class="card-title"><?php echo get_the_title(); ?></h5>
                            <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
                            <a href="<?php the_permalink(); ?>" class="btn btn-danger ml-5">VOIR LE PRODUIT</a>
                        </div>
                    </div>
                </div>
            <?php
            endwhile;
            ?>
        </div>
    </section>
    <div style="background-color: white;" class="fond">
        <sectionid="entrees" class="mb-5">
            <h2>Entrées</h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'bocaux',
                    'showposts' => 3,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'genre',
                            'field'    => 'slug',
                            'terms'    => 'entree',
                        ),
                    ),
                );
                $the_query = new WP_Query($args);
                while ($the_query->have_posts()) :
                    $the_query->the_post();
                ?>
                    <div class="mt-3 col-md-4">
                        <div class="card border-0" style="width: 18rem;">
                            <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo get_the_title(); ?></h5>
                                <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
                                <a href="<?php the_permalink(); ?>" class="btn btn-danger ml-5">VOIR LE PRODUIT</a>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
            </section>
            <section id="plats" class="mb-5">
                <h2>Plats</h2>
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'bocaux',
                        'showposts' => 3,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'genre',
                                'field'    => 'slug',
                                'terms'    => 'plat',
                            ),
                        ),
                    );
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) :
                        $the_query->the_post();
                    ?>
                        <div class="mt-3 col-md-4">
                            <div class="card border-0" style="width: 18rem;">
                                <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo get_the_title(); ?></h5>
                                    <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-danger ml-5">VOIR LE PRODUIT</a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            </section>
            <section id="desserts" class="mb-5">
                <h2>Desserts</h2>
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'bocaux',
                        'showposts' => 3,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'genre',
                                'field'    => 'slug',
                                'terms'    => 'dessert',
                            ),
                        ),
                    );
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) :
                        $the_query->the_post();
                    ?>
                        <div class="mt-3 col-md-4">
                            <div class="card border-0" style="width: 18rem;">
                                <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo get_the_title(); ?></h5>
                                    <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-danger ml-5">VOIR LE PRODUIT</a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            </section>
            <section id="epicerie" class="mb-5">
                <h2>Epicerie</h2>
                <div class="row">
                    <?php
                    $args = array(
                        'post_type' => 'bocaux',
                        'showposts' => 12,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'genre',
                                'field'    => 'slug',
                                'terms'    => 'epicerie',
                            ),
                        ),
                    );
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) :
                        $the_query->the_post();
                    ?>
                        <div class="mt-3 col-md-3">
                            <div class="card border-0" style="width: 10rem;">
                                <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo get_the_title(); ?></h5>
                                    <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-danger ml-5">Voir le produit</a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            </section>
    </div>
</div>

<?php get_footer(); ?>