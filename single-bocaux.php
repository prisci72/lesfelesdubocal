<?php get_header();?>

?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


<div class="container">
    <div class="row text-uppercase mb-5">
        <nav class="nav mt-3">
            <a class="nav-link text-dark border border-dark rounded-pill mr-5" href="http://localhost/Les-f%C3%A9l%C3%A9s-du-bocal/bocaux/"> < RETOUR</a>
            <a class="nav-link text-dark ml-5" href="front-page.php">ACCUEIL</a>
            <a class="nav-link text-dark" href="page-plats.php"><?php the_terms(get_the_ID(), 'genre'); ?></a>
            <a class="nav-link text-dark" href="#"><?php echo get_the_title(); ?></a>
        </nav>
    </div>
    <div class="row">
        <?php

        if (have_posts()) {
            while (have_posts()) {
                the_post();
                $genre = get_the_terms(get_the_ID(), 'genre');

        ?>
                <div class="card mb-3 border-0" style="max-width: 1140px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img rounded" alt="...">'; ?>
                        </div>
                        <div style="background-color:lemonchiffon" class="col-md-8">
                            <div style="background-color:lemonchiffon" class="card-body">
                                <h4 class="card-title"><?php the_terms(get_the_ID(), 'genre'); ?></h4>
                                <h2 class="card-title"><?php echo get_the_title(); ?></h2>
                                <p class="card-text"><?php echo get_the_content(); ?></p>
                                <h3 class="card-title text-danger"><?php echo get_the_excerpt(); ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
<?php
            }
        }
?>
<div class="row">
    <h2 class="mt-4">Vous pourrez aimer aussi</h2>
</div>
<div class="row">
        <?php
$args = array(
    'post_type'=> 'bocaux',
    'showposts'=> 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'genre',
            'field'    => 'slug',
            'terms'    => $genre[0]->slug,
        ),
    ),
);
$the_query = new WP_Query($args);
while($the_query->have_posts()):
    $the_query->the_post();
                ?>
                <div class="mt-3 col-md-4">
    <div class="card border-0" style="width: 18rem;">
    <?php echo '<img src="' . get_the_post_thumbnail_url() . '" class="card-img" alt="...">'; ?>
        <div class="card-body">
            <h5 class="card-title"><?php echo get_the_title(); ?></h5>
            <a href="#" class="card-link text-danger"><?php echo get_the_excerpt(); ?></a>
            <a href="<?php the_permalink();?>" class="btn btn-danger ml-5">VOIR LE PRODUIT</a>
        </div>
    </div>
</div>
                <?php
            endwhile;
        ?>
        </div>


</div>
<?php get_footer();?>
