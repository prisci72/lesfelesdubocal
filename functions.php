<?php

if (!function_exists("addStyle")) {
    function addStyle()
    {
        
        wp_enqueue_style("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css");
        wp_enqueue_style("font-awesome", "https://use.fontawesome.com/releases/v5.6.3/css/all.css");
        wp_enqueue_script("bootstrap", "https://code.jquery.com/jquery-3.2.1.slim.min.js");
        wp_enqueue_script("bootstrap", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js");
        wp_enqueue_script("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
        wp_enqueue_style("my-stylesheet", get_stylesheet_directory_uri() . '/style.css');
    }
}
add_action("wp_enqueue_scripts", "addStyle");


// Ajout de la cpt bocaux

if(!function_exists('bocaux_cpt')) {
    function bocaux_cpt() {
        $labelsbocal = array(
            "name"                  =>__ ("Bocaux", "enssop"),
            "singular_name"         =>__ ("Bocal", "enssop"),
            "menu_name"             =>__ ("Bocaux", "enssop"),
            "all_items"             =>__ ("Tous les bocaux", "enssop"),
            "add_new"               =>__ ("Ajouter un bocal", "enssop"),
            "add_new_item"          =>__ ("Ajouter un nouveau bocal", "enssop"),
            "edit_item"             =>__ ("Ajouter un bocal", "enssop"),
            "new_item"              =>__ ("Nouveau bocal", "enssop"),
            "view_item"             =>__ ("Voir le bocal", "enssop"),
            "view_items"            =>__ ("Voir les bocaux", "enssop"),
            "search_items"          =>__ ("Chercher un bocal", "enssop"),
            "not_found"             =>__ ("Pas de bocal trouvé", "enssop"),
            "not_found_in_trash"    =>__ ("Pas de bocal trouvé dans la corbeille", "enssop"),
            "featured_image"        =>__ ("Image mise en avant pour ce bocal", "enssop"),
            "set_featured_image"    =>__ ("Définir l'image mise en avant pour ce bocal", "enssop"),
            "remove_featured_image" =>__ ("Supprimer l'image mise en avant pour ce bocal", "enssop"),
            "use_featured_image"    =>__ ("Utiliser comme image mise en avant pour ce bocal", "enssop"),
            "archives"              =>__ ("Type de bocal", "enssop"),
            "insert_into_item"      =>__ ("Ajouter au bocaux", "enssop"),
            "uploaded_to_this_item" =>__ ("Ajouter au bocaux", "enssop"),
            "filter_items_list"     =>__ ("Filtrer la liste de bocaux", "enssop"),
            "items_list_navigation" =>__ ("Naviguer dans la liste de bocaux", "enssop"),
            "items_list"            =>__ ("Liste de bocaux", "enssop"),
            "attributes"            =>__ ("Paramètres du bocal", "enssop"),
            "name_admin_bar"        =>__ ("bocaux", "enssop"),
        );
        $argsbocal = array(
            "label"     =>__ ('bocaux', 'enssop'),
            "labels"    => $labelsbocal,
            "description"   =>  __('bocaux du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "bocaux", "with_front" => true),
            "query_var"             => 'bocaux',
            "menu_icon"             => "dashicons-book-alt",
            "supports"              => array("title", 'excerpt', 'editor', 'thumbnail'),
        );
    register_post_type( 'bocaux', $argsbocal );
    }
}
add_action( "init", "bocaux_cpt" );

// Ajout de la taxonomie genre

if( !function_exists( 'bocaux_taxo')){
    function bocaux_taxo(){

    /**
     * Taxonomy : Genre
     */

    $labels = array(
        'name'              => __( 'Genres', 'enssop' ),
        'singular_name'     => __( 'Genre', 'enssop' ),
        'search_items'      =>  __( 'Rechercher un genre', 'enssop' ),
        'all_items'         => __( 'Tous les genres', 'enssop' ),
        'parent_item'       => __( 'Genre Parent', 'enssop' ),
        'parent_item_colon' => __( 'Genre Parent :', 'enssop' ),
        'edit_item'         => __( 'Modifier le genre', 'enssop' ),
        'update_item'       => __( 'Modifier le genre', 'enssop' ),
        'add_new_item'      => __( 'Ajouter un genre' , 'enssop'),
        'new_item_name'     => __( 'Nouveau genre', 'enssop' ),
        'menu_name'         => __( 'Genre' , 'enssop' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' =>  true,
        'query_var'         => true,
        'rewrite'           => array( 'slug'    =>  'genre'),
    );
    register_taxonomy( 'genre', array( 'bocaux' ), $args);

}
}
add_action('init', 'bocaux_taxo');

if(!function_exists('avis_cpt')) {
    function avis_cpt() {
        $labelsAvis = array(
            "name"                  => __("Avis", "enssop"),
            "singular_name"         => __("Avis", "enssop"),
            "menu_name"             => __("Avis", "enssop"),
            "all_items"             => __("Tous les avis", "enssop"),
            "add_new"               => __("Ajouter un avis", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau avis", "enssop"),
            "edit_item"             => __("Ajouter un avis", "enssop"),
            "new_item"              => __("Nouveau avis", "enssop"),
            "view_item"             => __("Voir le avis", "enssop"),
            "view_items"            => __("Voir les avis", "enssop"),
            "search_items"          => __("Chercher un avis", "enssop"),
            "not_found"             => __("Pas de avis trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de avis trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour cet avis", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour cet avis", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour cet avis", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour cet avis", "enssop"),
            "archives"              => __("Type d'avis", "enssop"),
            "insert_into_item"      => __("Ajouter au avis", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au avis", "enssop"),
            "filter_items_list"     => __("Filtrer la liste d'avis", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste d'avis", "enssop"),
            "items_list"            => __("Liste des avis", "enssop"),
            "attributes"            => __("Paramètres du avis", "enssop"),
            "name_admin_bar"        => __("avis", "enssop"),
        );
        $argsAvis = array(
            "label"     =>__ ('avis', 'enssop'),
            "labels"    => $labelsAvis,
            "description"   =>  __('avis du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "avis", "with_front" => true),
            "query_var"             => 'avis',
            "menu_icon"             => "dashicons-smiley",
            "supports"              => array("title", 'editor', 'thumbnail',"nom client"),
        );
    register_post_type( 'avis', $argsAvis);
    }
}
add_action( "init", "avis_cpt" );

if(!function_exists('avis_cpt')) {
    function avis_cpt() {
        $labelsAvis = array(
            "name"                  => __("Avis", "enssop"),
            "singular_name"         => __("Avis", "enssop"),
            "menu_name"             => __("Avis", "enssop"),
            "all_items"             => __("Tous les avis", "enssop"),
            "add_new"               => __("Ajouter un avis", "enssop"),
            "add_new_item"          => __("Ajouter un nouveau avis", "enssop"),
            "edit_item"             => __("Ajouter un avis", "enssop"),
            "new_item"              => __("Nouveau avis", "enssop"),
            "view_item"             => __("Voir le avis", "enssop"),
            "view_items"            => __("Voir les avis", "enssop"),
            "search_items"          => __("Chercher un avis", "enssop"),
            "not_found"             => __("Pas de avis trouvé", "enssop"),
            "not_found_in_trash"    => __("Pas de avis trouvé dans la corbeille", "enssop"),
            "featured_image"        => __("Image mise en avant pour cet avis", "enssop"),
            "set_featured_image"    => __("Définir l'image mise en avant pour cet avis", "enssop"),
            "remove_featured_image" => __("Supprimer l'image mise en avant pour cet avis", "enssop"),
            "use_featured_image"    => __("Utiliser comme image mise en avant pour cet avis", "enssop"),
            "archives"              => __("Type d'avis", "enssop"),
            "insert_into_item"      => __("Ajouter au avis", "enssop"),
            "uploaded_to_this_item" => __("Ajouter au avis", "enssop"),
            "filter_items_list"     => __("Filtrer la liste d'avis", "enssop"),
            "items_list_navigation" => __("Naviguer dans la liste d'avis", "enssop"),
            "items_list"            => __("Liste des avis", "enssop"),
            "attributes"            => __("Paramètres du avis", "enssop"),
            "name_admin_bar"        => __("avis", "enssop"),
        );
        $argsAvis = array(
            "label"     => __('avis', 'enssop'),
            "labels"    => $labelsAvis,
            "description"   =>  __('avis du site', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array("slug" => "avis", "with_front" => true),
            "query_var"             => 'avis',
            "menu_icon"             => "dashicons-smiley",
            "supports"              => array("title", 'editor', 'thumbnail','nom client'),
        );
    register_post_type( 'avis', $argsAvis);
    }
}
add_action( "init", "avis_cpt" );


// Ajout de l'affichage des images mises en avant des publication du site
add_theme_support( 'post-thumbnails' );

register_nav_menus( [
    "nav_menu"=> __('Menu principal', 'enssop'),
    "footer_menu" => __('Menu de pied de page', 'enssop')
] );

