<?php get_header(); ?>

<div class="card" style="left:250px;padding:100px;color:white;width:66rem;
                                margin:120px;border:0px;
                                background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/sal-crevette-cabillaud-reverse.jpg');
                                background-repeat:no-repeat;">
    <div class="card-body">
        <h5 style="font-weight: bold;" class="card-title">Bocaux</h5>
        <h5 style="font-weight: bold;" class=" card-title">Locaux</h5>
        <p class="card-text"> Le restaurant Les Fêlés du Bocal vous <br> propose des produits frais, locaux, <br>servis dans des bocaux.</p>
        <p style="font-weight:bold" class="card-text">Restauration Bio | Sur place, à emporter <br> et prochainement en livraison.</p>
        <a style="border-radius: 20px;background-color:red;" href="#" class="btn btn-primary">JE DECOUVRE</a>
    </div>
</div>

<div class="card-group">
    <div style="border:0px;margin: 40px;height:200px" class="card">
        <div style="border:0px" class="card-body">
            <h5 style="color: red;font-weight:bold" class="card-title">Du bon, du bio, du bocal</h5>
            <p class="card-text">En verre et pour le goût, le choix de la <br>transparence pour une conservation <br>optimale des produits
                et de leurs saveurs.<br>Et puis c est bien connu, <br>plaisir des pupilles appelle bonheur <br> des papilles. </p>
        </div>
    </div>
    <div style="border:0px;margin: 40px;height:200px" class="card">
        <div class="card-body">
            <h5 style="color: red;font-weight:bold" class="card-title">Qualité 1 - 0 Déchet </h5>
            <p class="card-text">Parce que ça peut marcher, nous <br>misons sur la victoire de la <br>gourmandise contre le gaspillage.<br>
                Oui, si les bocaux nous emballent pour <br>leurs qualités c'est aussi parce qu'ils <br>nous épargnent quantité d'emballage.
            </p>
        </div>
    </div>
    <div style="border:0px;margin: 40px;height:200px" class="card">
        <div class="card-body">
            <h5 style="color: red;font-weight:bold" class="card-title">Une seule consigne</h5>
            <p class="card-text">Prolonger le plaisir gustatif. Échanger<br> avec le chef, toujours friand de retours <br>sur sa cuisine artisanale.<br>
                Accessoirement : <br>
                1 bocal restitué = 1€ <br>
                à valoir sur votre prochain repas.
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div style="border:0px;background-color:lemonchiffon;margin:100px" class=" card">
            <div class="card-body">
                <h5 style="font-weight:bold" card-title>Au menu <br>
                    aujourd'hui </h5>
                <p class="card-text">Entre le chef et vous, il n'y a qu'une vitre. Celle <br>de votre écran. Parce qu'une image
                    vaut mille<br> mots et parce que vous n'avez pas froid aux <br>yeux, retrouvez ici au quotidien l'insta-show <br>des Fêlés du Bocal
                    : toutes les recettes et <br>menus du jour.
                </p>
                <a style="border-radius:30px; background-color:red" href="#" class="btn btn-primary">JE DECOUVRE LES RECETTES</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/sal-crevette-cabillaud-reverse.jpg);
                                    background-repeat:no-repeat;
                                    width: 55rem; height: 40rem" class="card">
            <p style="border-radius:10px;background-color:lemonchiffon;width:180px;height:30px;text-align:center;
                                        margin-top: 30px; margin-left: 20px;">MARDI 30 JUILLET </p>

        </div>
    </div>
</div>
</div>

<div style="background-color: white;" class="row">
    <div class="col-sm-6">
        <div style="border: 0px;" class="card">
            <div class="card-body">
                <h3 style="font-weight: bold;margin-left:150px;padding:20px" class="card-title">Ils se sont régalés </h3>
                <a style="background-color: red;color:white;border-radius:30px;margin-left:180px" href="#" class="btn btn-primary">VOIR TOUS LES AVIS</a>
            </div>
        </div>
    </div>
    <?php
    $args = array(
        'post_type' => 'avis',
        'showposts' => 5,
    );
    $the_query = new WP_Query($args);
    while ($the_query->have_posts()) :
        $the_query->the_post();
    ?>
        <div class="col-sm-3">
            <div style="border: 0px;" class="card">
                <div class="card-body">
                    <h5 style="color: red;font-weight:bold;font-size:80px" class="card-title"></h5>
                    <p class="card-text"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/text-quotes-.svg"><?php echo ' ' . get_the_content(); ?></p>
                    <p style="font-weight:bold"><?php echo get_the_title(); ?></p>
                </div>
            </div>
        </div>
    <?php
    endwhile;
    ?>
</div>
</div>

<div class="card mb-3">
    <div style="background-color: lightblue;text-align:center" class="card-body">
        <h5 class="card-title">Suivez-nous sur :
            <i class="fa fa-instagram" id="insta" aria-hidden="true"></i>
            <i class="fa fa-facebook" id="facebook" aria-hidden="true"></i></h5>



    </div>

    <?php get_footer(); ?>