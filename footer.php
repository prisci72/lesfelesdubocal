<link href="style.css" rel="stylesheet">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

<footer style="background-color: lemonchiffon;">
    <div class="footer">
        <div class="row">
            <div class="col-sm-4">
                <div style="background-color: lemonchiffon;border:0px" class="card">
                    <div class="card-body">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/logo.png" class="attachment-full size-full">
                        <p class="card-text "><br><br><br>
                            <h3 style="font-size: 17px;margin-left:300px">Suivez nous sur les résaux : <i class="fa fa-instagram" id="insta" aria-hidden="true"></i>
                                <i class="fa fa-facebook" id="facebook" aria-hidden="true"></i></h3>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div style="background-color: lemonchiffon;border:0px" class="card">
                    <div class="card-body">
                        <p class="card-text">02 43 78 25 11<br>alexandre@les-feles-du-bocal.bio </p>
                        <p class="card-text">198 rue Nationale <br>Place Washington<br>72000 Le Mans</p>

                        
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div style="color: black;border:0px" class="card">
                    <div style="background-color: lemonchiffon;" class="card-body">
                        <ul class=" footer-text">
                            <a style="color: black;" class="nav-link" href="#">Concept </a>
                            <a style="color: black;" class="nav-link" href="#">PLats du jour</a>
                            <a style="color: black;" class="nav-link " href="#"> Palette de saveurs</a>
                            <a style="color: black;" class="nav-link " href="#"> Horaires</a>
                            <a style="color: black;" class="nav-link " href="#">Contact</a>

                            </li>

                        </ul>

                        <a style="border-radius: 30px;background-color:red;color:white;width: 15rem;
                        height: 3rem;
                        text-align: center;
                        padding: 10px;
                        margin-left:20px" href="boutique.php" class="bouton" role="button">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-telephone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M3.925 1.745a.636.636 0 0 0-.951-.059l-.97.97c-.453.453-.62 1.095-.421 1.658A16.47 16.47 0 0 0 5.49 10.51a16.471 16.471 0 0 0 6.196 3.907c.563.198 1.205.032 1.658-.421l.97-.97a.636.636 0 0 0-.06-.951l-2.162-1.682a.636.636 0 0 0-.544-.115l-2.052.513a1.636 1.636 0 0 1-1.554-.43L5.64 8.058a1.636 1.636 0 0 1-.43-1.554l.513-2.052a.636.636 0 0 0-.115-.544L3.925 1.745zM2.267.98a1.636 1.636 0 0 1 2.448.153l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z" />
                            </svg>
                            <span class="bouton">JE COMMANDE</span></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div style="border: 0px;margin:30px;" class="commande">
        <a style="color:black" href="boutique.php"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imageBocal/Groupe 778.svg"><br>JE COMMANDE <br> PAR TELEPHONE</a>

    </div>

</footer>
<!-- ./wrapper -->

<!-- jQuery -->

<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>


</html>